#include <stdio.h>
#include <time.h>

int main()
{
	char CapLet[27];
	char SmallLet[27];
	char Digits[] = "0123456789\0";
	int i,j;
	
	for (i = 0; i <= 25; i++)
	{
		CapLet[i] = i+65;
		SmallLet[i] = i + 97;
	}
	CapLet[26] = '\0';
	SmallLet[26] = '\0';

	puts("Passwords:\n");
	srand((int)time(NULL));
	i = 0;
	while(i < 10)
	{
		j = 0;
		while (j < 8)
		{
			switch (rand() % 3)
			{
			case 0:
				printf("%c", CapLet[rand()%26]);
				break;
			case 1:
				printf("%c", SmallLet[rand()%26]);
				break;
			case 2:
				printf("%c", Digits[rand() % 9]);
				break;
			default:
				break;
			}
			j++;
		}
		printf("\n");
		i++;
	}

	return 0;
}