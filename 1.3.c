#include <stdio.h>

float value;
char type[1];

int main()
{
	while (type[0] != 'd' && type[0] != 'r' && type[0] != 'D' && type[0] != 'R')
	{
		printf("Please enter size of an angle in degrees or radians\nLike this: 45.00R or 45.00D\n");
		scanf_s("%f%c", &value, type, sizeof(value), sizeof(type));
	};

	if (type[0] == 'r' || type[0] == 'R')
	{
		value = value * 180 / 3.1415926;
		printf("This angle in degrees is equal to %f\n", value);
	}

	if (type[0] == 'd' || type[0] == 'D')
	{
		value = value / 180.00 * 3.1415926;
		printf("This angle in radians is equal to %f\n", value);
	}
	return 0;
}