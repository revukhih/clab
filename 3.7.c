#include <stdio.h>
#include <time.h>

int main()
{
	int table[128][2] = { 0 };
	char str[256];
	int i = 0;

	puts("Enter a string, please");
	fgets(str, 256, stdin);

	for (i = 0; i < (int)strlen(str); i++)
	{
		table[str[i]][0] = str[i];
		table[str[i]][1]++;
	}

	for (i = 32; i < 128; i++)
	{
		if (table[i][1]>0)
		{
			printf("%c %d\n", (char)table[i][0], table[i][1]);
		}
	}


	int tempvar = 0;
	int tempvar_symbol = 0;

	for (int i = 0; i <= 127; i++)
	{
		for (int j = 0; j <= 127 - i; j++)
		{
			if (table[j][1]<table[j + 1][1])
			{
				tempvar = table[j][1];
				tempvar_symbol = table[j][0];
				table[j][0] = table[j + 1][0];
				table[j][1] = table[j + 1][1];
				table[j + 1][1] = tempvar;
				table[j + 1][0] = tempvar_symbol;
			}
		}
	}
	puts("Sorted table");
	for (i = 0; i < 128; i++)
	{
		if (table[i][1]>0 && table[i][0]!='\n')
		{
			printf("%c %d\n", (char)table[i][0], table[i][1]);
		}
	}

	return 0;
}