#include <stdio.h>
#include <time.h>

int main()
{
	float height = 5000;
	float path = 0;
	int time_loc = 0;
	const float g = 9.81f;

	puts("Please enter height H:");
	scanf_s("%f", &height, sizeof(height));
	while (height - path >= 0)
	{
		srand((int)time(0));
		clock_t begin = clock();
		while (clock() < begin + 1000);
		printf("t=%02d c h=%.1f m\n", time_loc, height - path);
		time_loc++;
		path = g*time_loc*time_loc / 2;

		if (height - path<0)
			puts("BABAH");
	}
	return 0;
}