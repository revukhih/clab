#include <stdio.h>
#include <time.h>

int main()
{
	int number;
	int input;
	srand((int)time(NULL));
	number = rand() % 100 + 1;
	do {
		puts("Guess the number (1 to 100):");
		scanf_s("%d", &input, sizeof(input));
		if (number > input)
			puts("The number is higher");
		if (number < input)
			puts("The number is less");
	} while (number != input);
	puts("That is correct!");
	
	return 0;
}