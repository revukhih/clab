#include <stdio.h>
#include <stdlib.h>

int compare(const void *a, const void *b)
{

	return strlen(*(char**)a) - strlen(*(char**)b);
}

int main()
{
	char arr[256][256];
	FILE *in,*out;
	char *p[256] = { 0 };
	int i = 0;
	int count = 0;
	
	fopen_s(&in,"in.txt","rt");
	if (in == NULL)
	{
		perror("File error!");
		return 1;
	}
	
	while (fgets(arr[i], 256, in))
	{
		p[i] = arr[i];
		if (*arr[i] == '\n')
			break;
		i++;
	}
	fclose(in);
	count = i;
	qsort(p, count, sizeof(char*), compare);

	fopen_s(&out, "out.txt", "wt");
	if (out == NULL)
	{
		perror("File error!");
		return 1;
	}

	for (i = 0; i<count; i++)
	{
		fprintf_s(out,"%s", p[i]);
	}
	fclose(out);

	return 0;
}