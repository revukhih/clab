#include <stdio.h>
#include <time.h>


int main()
{
	int size = 10;
	int arr[10] = {0};
	int i = 0;
	int j = 0;
	int k = 0;
	int sum = 0;

	srand((int)time(NULL));

	for (i = 0; i < size; i++)
	{
		arr[i] = rand() % 100 + 1;
		if (i % 2)
			arr[i] = -arr[i];
	}
	printf("Array:\n");
	for (i = 0; i < size; i++)
		printf("%d ", arr[i]);
	printf("\n");

	for (j = size; j>0; j--)
	{
		if (arr[j] > 0)
			break;
	}

	for (i = 0; i < size; i++)
	{
		if (arr[i] < 0)
			break;
	}
	for (k = i+1; k < j;k++)
	{
		sum += arr[k];
	}

	printf("%d\n", sum);

	return 0;
}