#include <stdio.h>
#include <stdlib.h>

int compare(const void *a, const void *b)
{
	
	return strlen(*(char**)a) - strlen(*(char**)b);
}

int main()
{
	char arr[256][256];
	char *p[256] = {0};
	int i = 0;
	int count = 0;
	puts("Enter some strings, please:");
	for (i = 0; i < 10; i++)
	{
		fgets(arr[i], 256, stdin);
		p[i] = arr[i];
		if (*arr[i] == '\n')
			break;
	}
	count = i;
	qsort(p, count, sizeof(char*), compare);

	puts("Strings sorted by length");
	for (i = 0; i<count; i++)
	{
		printf("%s", p[i]);
	}

		
	return 0;
} 